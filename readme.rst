Sobre EPTC-2-Maps
=================

EPTC-2-Maps é um sistema simples para converter as rotas dos ônibus de
Porto Alegre, providos pela EPTC (Empresa Portoalegrense de Transporte e
Circulação) para rotas do Google Maps, permitindo a visualização dos mesmos.

Scrapper
--------
Para carregar os dados, foi criado um web scrapper em Python. Como as rotas
não são atualizadas com freqüência, não há problema em não se utilizar o
site da EPTC.