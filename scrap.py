#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib
import urllib2
import cookielib
import logging
import pprint
import time
import cPickle
import json

from BeautifulSoup import BeautifulSoup

def request_main():
    """Request the main data. The page have the route names and codes."""
    # http://www.eptc.com.br/EPTC_Itinerarios/linha.asp
    logging.debug('Starting request...')
    request = urllib2.Request(
        url='http://www.eptc.com.br/EPTC_Itinerarios/linha.asp'
    )
    conn = urllib2.urlopen(request)
    logging.debug('Reading response...')
    #logging.debug('Info:\n%s', pprint.pformat(conn.info().items()))
    content = conn.read()
    logging.debug('Done.')
    return content
    
def parse_main(content):
    """Parse the main content, looking for the options. Returns a dictionary
    with the options and the names of each route."""
    logging.debug('Parsing content...')
    soup = BeautifulSoup(content)
    options = soup.findAll('option')
    result = {}
    for option in options:
        result[option['value']] = ' '.join(option.string.split())
    #logging.debug(pprint.pformat(result))
    return result;
    
def request_route(route):
    """Request a single route page. Returns the content of the page."""
    logging.debug('Requesting route %s...', route)
    post = urllib.urlencode({
        'Action': 'Nada',
        'Linha': route,
        'Logradouro': 0,
        'Sentido': 0,
        'Tipo': 'I',
        'Veiculo': 1
    })
    req = urllib2.Request(
        url='http://www.eptc.com.br/EPTC_Itinerarios/Cadastro.asp',
        data=post
    )
    conn = urllib2.urlopen(req)
    #logging.debug(pprint.pformat(conn.info()))
    data = conn.read()
    return data

def parse_route(routeid, name, content):
    """Parse the contents of a route. Returns a dictionary with the
    route direction and street names."""
    result = []
    
    logging.debug("Parsing route %s (%d bytes)...", name, len(content))
    #logging.debug(content.decode('ascii', 'ignore'))
    soup = BeautifulSoup(content)
    tables = soup.findAll('table')
    for table in tables:
        direction = ' '.join(table.previousSibling.div.b.string.split())
        info = {
            'direction': direction,
            'streets': []
        }
        #logging.debug('Direction ==> %s', direction.encode('ascii', 'ignore'))
        for row in table.findAll('td'):
            address = ' '.join(row.div.b.string.split())
            #logging.debug(address.encode('ascii', 'ignore'))
            info['streets'].append(address)
        result.append(info)
    return result

def create_cookiejar():
    """Create a cookie jar and set up the cookie handler."""
    cookieJar = cookielib.CookieJar()
    cookieHandler = urllib2.HTTPCookieProcessor(cookieJar)

    cookieHandler = urllib2.build_opener(cookieHandler)
    urllib2.install_opener(cookieHandler)
    return

def get_routes():
    """Retrieve bus lines and their routes."""
    all_routes = {}
    options = parse_main(request_main())
    for routeid in options:
        data = request_route(routeid)
        route = parse_route(routeid, options[routeid], data)
        #logging.debug(pprint.pformat(route))
        all_routes[routeid] = {
               'name': options[routeid],
               'directions': route
               }
        cPickle.dump(all_routes, open('routes.pickle', 'wb'))
        logging.debug('Sleeping 20 seconds...')
        time.sleep(20)  # need to sleep to avoid crashing the server
    return all_routes

def main():
    logging.basicConfig(level=logging.DEBUG)
    all_routes = get_routes()
    # logging.debug(pprint.pformat(all_routes))
    open('routes.json').write(json.dumps(all_routes))

if __name__ == '__main__':
	main()